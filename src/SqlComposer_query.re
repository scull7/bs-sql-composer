module type QueryType = {
  module Delete: {
    type t;

    let make: unit => t;

    let modifier: (t, [ | `LowPriority | `Quick | `Ignore]) => t;

    let from: (t, string) => t;

    let where: (t, string) => t;

    let orderBy: (t, [ | `Asc(string) | `Desc(string)]) => t;

    let limit: (t, ~offset: int=?, ~row_count: int) => t;

    let toSql: t => string;
  };

  module Select: {
    type t;

    let make: unit => t;

    let modifier:
      (
        t,
        [
          | `Distinct
          | `HighPriority
          | `StraightJoin
          | `MySqlNoCache
          | `MySqlCalcFoundRows
        ]
      ) =>
      t;

    let field: (t, string) => t;

    let from: (t, string) => t;

    let join: (t, string) => t;

    let where: (t, string) => t;

    let orderBy: (t, [ | `Asc(string) | `Desc(string)]) => t;

    let groupBy: (t, string) => t;

    let limit: (t, ~offset: int=?, ~row_count: int) => t;

    let toSql: t => string;
  };

  module Update: {
    type t;

    let modifier: (t, [ | `LowPriority | `Ignore]) => t;

    let make: unit => t;

    let from: (t, string) => t;

    let join: (t, string) => t;

    let set: (t, string, string) => t;

    let where: (t, string) => t;

    let orderBy: (t, [ | `Asc(string) | `Desc(string)]) => t;

    let limit: (t, ~offset: int=?, ~row_count: int) => t;

    let toSql: t => string;
  };

  module Conversion: {
    let deleteFromSelect: Select.t => Delete.t;

    let updateFromSelect: Select.t => Update.t;
  };
};

module Make = (StdLib: SqlComposer_stdlib.Type) : QueryType => {
  module Component = SqlComposer_component.Make(StdLib);

  module Sql = {
    type t = {
      fields: option(Component.Fields.t),
      from: option(Component.From.t),
      join: option(Component.Join.t),
      set: option(Component.Assignment.t),
      where: option(Component.Where.t),
      groupBy: option(Component.GroupBy.t),
      orderBy: option(Component.OrderBy.t),
      limit: option(Component.Limit.t),
    };

    let make = () => {
      fields: None,
      from: None,
      join: None,
      set: None,
      where: None,
      groupBy: None,
      orderBy: None,
      limit: None,
    };

    let assemble =
        (
          ~fields=?,
          ~from=?,
          ~join=?,
          ~set=?,
          ~where=?,
          ~groupBy=?,
          ~orderBy=?,
          ~limit=?,
          _,
        ) => {
      fields,
      from,
      join,
      set,
      where,
      groupBy,
      orderBy,
      limit,
    };

    module Clause = {
      type t =
        | Fields
        | From
        | Table
        | Join
        | Assignment
        | Where
        | GroupBy
        | OrderBy
        | Limit;
    };

    let fromGet = t => t.from;

    let joinGet = t => t.join;

    let whereGet = t => t.where;

    let orderByGet = t => t.orderBy;

    let limitGet = t => t.limit;

    let fields = (query, field) => {
      ...query,
      fields: Component.Fields.add(query.fields, field),
    };

    let from = (query, table) => {
      ...query,
      from: Some(Component.From.make(table)),
    };

    let join = (query, join) => {
      ...query,
      join: Component.Join.add(query.join, join),
    };

    let set = (query, field, value) => {
      ...query,
      set: Component.Assignment.add(query.set, field, value),
    };

    let where = (query, where) => {
      ...query,
      where: Component.Where.add(query.where, where),
    };

    let groupBy = (query, group) => {
      ...query,
      groupBy: Component.GroupBy.add(query.groupBy, group),
    };

    let orderBy = (query, order) => {
      ...query,
      orderBy: Component.OrderBy.add(query.orderBy, order),
    };

    let limit = (query, ~offset=?, ~row_count) => {
      ...query,
      limit:
        switch (offset) {
        | Some(o) => Some(Component.Limit.withOffset(row_count, o))
        | None => Some(Component.Limit.withCount(row_count))
        },
    };

    let renderClause = (t, clause) =>
      switch (clause) {
      | Clause.Fields => t.fields->Component.Fields.render
      | Clause.From => t.from->Component.From.render
      | Clause.Table => t.from->Component.From.raw
      | Clause.Join => t.join->Component.Join.render
      | Clause.Assignment => t.set->Component.Assignment.render
      | Clause.Where => t.where->Component.Where.render
      | Clause.GroupBy => t.groupBy->Component.GroupBy.render
      | Clause.OrderBy => t.orderBy->Component.OrderBy.render
      | Clause.Limit => t.limit->Component.Limit.render
      };

    let render = (~modifier=?, prefix, clauseList, t) => {
      [modifier, ...StdLib.List.map(clauseList, renderClause(t))]
      ->StdLib.List.keep(StdLib.Option.isSome)
      ->(StdLib.List.map(StdLib.Option.getExn))
      ->(x => StdLib.String.concat("\n", x)->(x => prefix ++ x));
    };
  };

  module Abstract = {
    module type ClauseType = {
      let prefix: string;
      let clauseList: list(Sql.Clause.t);
    };

    module Make = (FlagType: Component.FlagType, Impl: ClauseType) => {
      module Modifier = Component.Modifier(FlagType);

      type t = {
        modifier: option(Modifier.t),
        query: Sql.t,
      };

      let make = () => {modifier: None, query: Sql.make()};

      let assemble =
          (
            ~fields=?,
            ~from=?,
            ~join=?,
            ~where=?,
            ~groupBy=?,
            ~orderBy=?,
            ~limit=?,
            _,
          ) => {
        modifier: None,
        query:
          Sql.assemble(
            ~fields?,
            ~from?,
            ~join?,
            ~where?,
            ~groupBy?,
            ~orderBy?,
            ~limit?,
            (),
          ),
      };

      let modifier = (t, flag) => {
        modifier: Modifier.add(t.modifier, flag),
        query: t.query,
      };

      let hasModifier = t => t.modifier->StdLib.Option.isSome;

      let queryGet = t => t.query;

      let field = (t, field) => {...t, query: Sql.fields(t.query, field)};

      let from = (t, from) => {...t, query: Sql.from(t.query, from)};

      let join = (t, join) => {...t, query: Sql.join(t.query, join)};

      let set = (t, field, value) => {
        ...t,
        query: Sql.set(t.query, field, value),
      };

      let where = (t, where) => {...t, query: Sql.where(t.query, where)};

      let groupBy = (t, group) => {
        ...t,
        query: Sql.groupBy(t.query, group),
      };

      let orderBy = (t, order) => {
        ...t,
        query: Sql.orderBy(t.query, order),
      };

      let limit = (t, ~offset=?, ~row_count) => {
        ...t,
        query: Sql.limit(t.query, ~offset?, ~row_count),
      };

      let toSql = t => {
        let modifier = t.modifier->Modifier.render;
        let prefix = t->hasModifier ? Impl.prefix : Impl.prefix ++ "\n";

        Sql.render(~modifier?, prefix, Impl.clauseList, t.query);
      };
    };
  };

  module Delete =
    Abstract.Make(
      {
        type flag = [ | `LowPriority | `Quick | `Ignore];

        let toString =
          fun
          | `LowPriority => "LOW_PRIORITY"
          | `Quick => "QUICK"
          | `Ignore => "IGNORE";
      },
      {
        let prefix = "DELETE";

        let clauseList = [
          Sql.Clause.From,
          Sql.Clause.Where,
          Sql.Clause.OrderBy,
          Sql.Clause.Limit,
        ];
      },
    );

  module Select =
    Abstract.Make(
      {
        type flag = [
          | `Distinct
          | `HighPriority
          | `StraightJoin
          | `MySqlNoCache
          | `MySqlCalcFoundRows
        ];
        let toString =
          fun
          | `Distinct => "DISTINCT"
          | `HighPriority => "HIGH_PRIORITY"
          | `StraightJoin => "STRAIGHT_JOIN"
          | `MySqlNoCache => "SQL_NO_CACHE"
          | `MySqlCalcFoundRows => "SQL_CALC_FOUND_ROWS";
      },
      {
        let prefix = "SELECT";

        let clauseList = [
          Sql.Clause.Fields,
          Sql.Clause.From,
          Sql.Clause.Join,
          Sql.Clause.Where,
          Sql.Clause.GroupBy,
          Sql.Clause.OrderBy,
          Sql.Clause.Limit,
        ];
      },
    );

  module Update =
    Abstract.Make(
      {
        type flag = [ | `LowPriority | `Ignore];

        let toString =
          fun
          | `LowPriority => "LOW_PRIORITY"
          | `Ignore => "IGNORE";
      },
      {
        let prefix = "UPDATE";

        let clauseList = [
          Sql.Clause.Table,
          Sql.Clause.Join,
          Sql.Clause.Assignment,
          Sql.Clause.Where,
          Sql.Clause.OrderBy,
          Sql.Clause.Limit,
        ];
      },
    );

  module Conversion = {
    let deleteFromSelect = select => {
      let query = Select.queryGet(select);

      Delete.assemble(
        ~from=?query->Sql.fromGet,
        ~where=?query->Sql.whereGet,
        ~orderBy=?query->Sql.orderByGet,
        ~limit=?query->Sql.limitGet,
        (),
      );
    };

    let updateFromSelect = select => {
      let query = Select.queryGet(select);

      Update.assemble(
        ~from=?query->Sql.fromGet,
        ~join=?query->Sql.joinGet,
        ~where=?query->Sql.whereGet,
        ~orderBy=?query->Sql.orderByGet,
        ~limit=?query->Sql.limitGet,
        (),
      );
    };
  };
};
