module type Type = {
  module List: {
    type t('a) = list('a);

    let add: (list('a), 'a) => list('a);

    let keep: (list('a), 'a => bool) => list('a);

    let make: (int, 'a) => list('a);

    let map: (list('a), 'a => 'b) => list('b);

    let reverse: list('a) => list('a);
  };

  module Option: {
    let getExn: option('a) => 'a;

    let isSome: option('a) => bool;

    let map: (option('a), 'a => 'b) => option('b);

    let mapWithDefault: (option('a), 'b, 'a => 'b) => 'b;
  };

  module String: {let concat: (string, list(string)) => string;};
};
